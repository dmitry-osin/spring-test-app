package pro.osin.testproject.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.List;

@Entity
@Getter
@Setter
@ToString
@Table(name = "buildings")
public class BuildingEntity {

    @Id
    @Column(name = "id", nullable = false)
    @SequenceGenerator(name = "BUILDING_SEQ_GEN", sequenceName = "BUILDING_SEQ")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "BUILDING_SEQ_GEN")
    private Long id;

    @Column(name = "tree_id", nullable = false, unique = true)
    private Long treeId;

    @Column(name = "name", nullable = false, unique = true)
    private String name;

    @ToString.Exclude
    @OneToMany(cascade = CascadeType.PERSIST, orphanRemoval = true)
    @JoinColumn(name = "building_id")
    private List<CategoryEntity> categories;

}
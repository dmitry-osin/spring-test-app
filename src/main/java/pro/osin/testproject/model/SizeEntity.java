package pro.osin.testproject.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.List;

@Entity
@Getter
@Setter
@ToString
@Table(name = "sizes")
public class SizeEntity {

    @Id
    @Column(name = "id", nullable = false)
    @SequenceGenerator(name = "SIZE_SEQ_GEN", sequenceName = "SIZE_SEQ")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SIZE_SEQ_GEN")
    private Long id;

    @Column(name = "height", nullable = false)
    private Double height;

    @Column(name = "width", nullable = false)
    private Double width;

    @ToString.Exclude
    @OneToMany(cascade = CascadeType.PERSIST, orphanRemoval = true)
    @JoinColumn(name = "size_id")
    private List<ItemEntity> items;

}
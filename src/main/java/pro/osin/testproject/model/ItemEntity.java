package pro.osin.testproject.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Getter
@Setter
@ToString
@Table(name = "items")
public class ItemEntity {

    @Id
    @Column(name = "id", nullable = false)
    @SequenceGenerator(name = "ITEM_SEQ_GEN", sequenceName = "ITEM_SEQ")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ITEM_SEQ_GEN")
    private Long id;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "price", nullable = false, precision = 16, scale = 2)
    private BigDecimal price;

}
package pro.osin.testproject.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pro.osin.testproject.model.SubCategoryEntity;

public interface SubCategoryRepository extends JpaRepository<SubCategoryEntity, Long> {
}
package pro.osin.testproject.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pro.osin.testproject.model.ItemEntity;

public interface ItemRepository extends JpaRepository<ItemEntity, Long> {



}
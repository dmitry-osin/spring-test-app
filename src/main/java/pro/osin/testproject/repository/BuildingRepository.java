package pro.osin.testproject.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pro.osin.testproject.model.BuildingEntity;

public interface BuildingRepository extends JpaRepository<BuildingEntity, Long> {



}
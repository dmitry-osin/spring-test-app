package pro.osin.testproject.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pro.osin.testproject.model.CategoryEntity;

public interface CategoryRepository extends JpaRepository<CategoryEntity, Long> {



}
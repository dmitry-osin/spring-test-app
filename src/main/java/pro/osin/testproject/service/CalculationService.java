package pro.osin.testproject.service;

import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import pro.osin.testproject.rest.transfer.CalculationRequest;
import pro.osin.testproject.rest.transfer.CalculationResponse;

@Slf4j
@Service
public class CalculationService {

    public CalculationResponse calculatePrice(@NonNull CalculationRequest calculationRequest) {
        log.info("Calculation request for next data: {}", calculationRequest);
        return null;
    }

}

package pro.osin.testproject.rest.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import pro.osin.testproject.rest.transfer.CalculationRequest;
import pro.osin.testproject.rest.transfer.CalculationResponse;

@Slf4j
@RestController
public class CalculationController {

    @PostMapping
    public ResponseEntity<CalculationResponse> calculateGoods(CalculationRequest request) {
        log.info("Incoming request with next data: {}", request);
        return ResponseEntity.internalServerError().build(); // Not Implemented
    }

}

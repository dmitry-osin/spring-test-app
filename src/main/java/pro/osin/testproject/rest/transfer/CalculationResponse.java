package pro.osin.testproject.rest.transfer;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class CalculationResponse {
    private List<BuildingDto> buildings;
}

@Getter
@Setter
class BuildingDto {
    private long id;
    private CategoryDto category;
}

@Getter
@Setter
class CategoryDto {
    private String name;
    private SubCategoryDto subCategory;
}

@Getter
@Setter
class SubCategoryDto {
    private String name;
    private SizeDto size;
}

@Getter
@Setter
class SizeDto {
    private double height;
    private double width;
}

@Getter
@Setter
class ItemDto {
    private String name;
    private double price;
}

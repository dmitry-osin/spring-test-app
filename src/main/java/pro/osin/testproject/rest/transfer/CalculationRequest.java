package pro.osin.testproject.rest.transfer;


import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "tree_id",
        "prop"
})
@ToString
public class CalculationRequest {

    @JsonProperty("tree_id")
    public String treeId;
    @JsonProperty("prop")
    public String prop;

}

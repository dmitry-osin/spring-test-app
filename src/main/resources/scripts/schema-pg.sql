-- public.building_seq definition

-- DROP SEQUENCE public.building_seq;

CREATE SEQUENCE public.building_seq
    INCREMENT BY 50
    MINVALUE 1
    MAXVALUE 9223372036854775807
    START 1
    CACHE 1
    NO CYCLE;


-- public.category_seq definition

-- DROP SEQUENCE public.category_seq;

CREATE SEQUENCE public.category_seq
    INCREMENT BY 50
    MINVALUE 1
    MAXVALUE 9223372036854775807
    START 1
    CACHE 1
    NO CYCLE;


-- public.item_seq definition

-- DROP SEQUENCE public.item_seq;

CREATE SEQUENCE public.item_seq
    INCREMENT BY 50
    MINVALUE 1
    MAXVALUE 9223372036854775807
    START 1
    CACHE 1
    NO CYCLE;


-- public.size_seq definition

-- DROP SEQUENCE public.size_seq;

CREATE SEQUENCE public.size_seq
    INCREMENT BY 50
    MINVALUE 1
    MAXVALUE 9223372036854775807
    START 1
    CACHE 1
    NO CYCLE;


-- public.subcategory_seq definition

-- DROP SEQUENCE public.subcategory_seq;

CREATE SEQUENCE public.subcategory_seq
    INCREMENT BY 50
    MINVALUE 1
    MAXVALUE 9223372036854775807
    START 1
    CACHE 1
    NO CYCLE;

-- public.buildings definition

-- Drop table

-- DROP TABLE public.buildings;

CREATE TABLE public.buildings (
                                  id int8 NOT NULL,
                                  "name" varchar(255) NOT NULL,
                                  tree_id int8 NOT NULL,
                                  CONSTRAINT buildings_pkey PRIMARY KEY (id),
                                  CONSTRAINT uk_laj9ikg96t6ta4yy1uhvruif3 UNIQUE (tree_id),
                                  CONSTRAINT uk_lf8jhhsls7j5q6n95vlbnlhe7 UNIQUE (name)
);


-- public.categories definition

-- Drop table

-- DROP TABLE public.categories;

CREATE TABLE public.categories (
                                   id int8 NOT NULL,
                                   "name" varchar(255) NOT NULL,
                                   building_id int8 NULL,
                                   CONSTRAINT categories_pkey PRIMARY KEY (id),
                                   CONSTRAINT uk_t8o6pivur7nn124jehx7cygw5 UNIQUE (name),
                                   CONSTRAINT fk9oyt2quc5k94u1c4lyn8861a7 FOREIGN KEY (building_id) REFERENCES public.buildings(id)
);


-- public.sub_categories definition

-- Drop table

-- DROP TABLE public.sub_categories;

CREATE TABLE public.sub_categories (
                                       id int8 NOT NULL,
                                       "name" varchar(255) NOT NULL,
                                       category_id int8 NULL,
                                       CONSTRAINT sub_categories_pkey PRIMARY KEY (id),
                                       CONSTRAINT uk_5m83dk3c53lrusxi146xmlv6 UNIQUE (name),
                                       CONSTRAINT fkjwy7imy3rf6r99x48ydq45otw FOREIGN KEY (category_id) REFERENCES public.categories(id)
);


-- public.sizes definition

-- Drop table

-- DROP TABLE public.sizes;

CREATE TABLE public.sizes (
                              id int8 NOT NULL,
                              height float8 NOT NULL,
                              width float8 NOT NULL,
                              sub_category_id int8 NULL,
                              CONSTRAINT sizes_pkey PRIMARY KEY (id),
                              CONSTRAINT fkt8j0cgedllva0oryrg2i3cj84 FOREIGN KEY (sub_category_id) REFERENCES public.sub_categories(id)
);


-- public.items definition

-- Drop table

-- DROP TABLE public.items;

CREATE TABLE public.items (
                              id int8 NOT NULL,
                              "name" varchar(255) NOT NULL,
                              price numeric(16, 2) NOT NULL,
                              size_id int8 NULL,
                              CONSTRAINT items_pkey PRIMARY KEY (id),
                              CONSTRAINT fknm0peey8pjpxbjmqla075bgbs FOREIGN KEY (size_id) REFERENCES public.sizes(id)
);